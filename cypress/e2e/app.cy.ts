describe('Navigation', () => {
  it('should navigate to comics', () => {
    cy.visit('http://localhost:3000/');

    cy.get('h1').contains('Latest issues');
  });
});
