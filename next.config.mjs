/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'readcomicsonline.ru',
      },
    ],
  },
};

export default nextConfig;
