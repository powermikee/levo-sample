import type { AppProps } from 'next/app';
import Container from 'react-bootstrap/Container';
import Header from '@/components/Header/Header';
import { SWRConfig } from 'swr';
import { ErrorBoundary } from 'react-error-boundary';

import 'bootstrap/dist/css/bootstrap.min.css';
import '@/styles/globals.css';

function ErrorMessage({ error }: {error: Error}) {
  return (
    <div role="alert">
      <p>Something went wrong:</p>

      <pre>{error.message}</pre>
    </div>
  );
}

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ErrorBoundary
      FallbackComponent={ErrorMessage}
    >
      <SWRConfig
        value={{
          fetcher: (resource, init) => fetch(resource, init).then((res) => res.json()),
        }}
      >
        <Header />

        <Container>
          <Component {...pageProps} />
        </Container>
      </SWRConfig>
    </ErrorBoundary>
  );
}
