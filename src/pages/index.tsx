import Head from 'next/head';
import useSWR from 'swr';
import EpisodeList from '@/components/EpisodeList/EpisodeList';
import { ComicType } from './api/comics';

type DataType = {
  comics: ComicType[],
};

export default function Home() {
  const { data, error, isLoading } = useSWR<DataType, Error>('/api/comics');
  const comics = data?.comics;

  return (
    <>
      <Head>
        <title>Marvel Comics</title>
        <meta name="description" content="Marvel comics" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <EpisodeList comics={comics} error={error} isLoading={isLoading} />
      </main>
    </>
  );
}
