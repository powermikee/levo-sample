import sX from 'scrapper-x';
import axios from 'axios';
import type { NextApiRequest, NextApiResponse } from 'next';

export type ComicType = {
  title: string,
  link: string,
  img: string,
  slug: string,
};

type ComicsType = {
  success: boolean,
  comics?: Array<ComicType>,
  totalPages?: number,
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<ComicsType>) {
  try {
    const page = 1;
    const cat = 34;
    const config = {
      repeatItemGroup: '.col-sm-6',
      dataFormat: {
        title: {
          selector: 'h5',
          type: 'text',
        },
        link: {
          selector: 'a:first-child',
          type: 'attr:href',
        },
        img: {
          selector: 'img',
          type: 'attr:src',
        },
      },
    };

    const { data }: { data: string } = await axios(`https://readcomicsonline.ru/filterList?page=${page}&cat=${cat}&alpha=&sortBy=views&asc=false&author=&tag=`);
    const scrapedComics: ComicType[] = sX.scrape(data, config);

    const comics = scrapedComics.map((comic) => {
      const { link, img } = comic;
      const slug = link.split('https://readcomicsonline.ru/comic/')[1];
      const fullImg = `https:${img}`;

      return {
        ...comic,
        slug,
        img: fullImg,
      };
    });

    res.status(200).json({
      success: true,
      comics,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
    });
  }
}
