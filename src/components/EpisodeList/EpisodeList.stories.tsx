import EpisodeListComp from './EpisodeList';

const config = {
  title: 'Components/Episode List',
  component: EpisodeListComp,
};

export default config;

export const comics = [
  {
    title: 'Amazing Spider-Man (2018-)',
    link: 'https://readcomicsonline.ru/comic/amazing-spiderman-2018',
    img: 'https://readcomicsonline.ru/uploads/manga/amazing-spiderman-2018/cover/cover_250x350.jpg',
    slug: 'amazing-spiderman-2018',
  },
  {
    title: 'Darth Vader (2017-)',
    link: 'https://readcomicsonline.ru/comic/darth-vader-2017',
    img: 'https://readcomicsonline.ru/uploads/manga/darth-vader-2017/cover/cover_250x350.jpg',
    slug: 'darth-vader-2017',
  },
  {
    title: 'Star Wars: Darth Vader (2020-)',
    link: 'https://readcomicsonline.ru/comic/star-wars-darth-vader-2020',
    img: 'https://readcomicsonline.ru/uploads/manga/star-wars-darth-vader-2020/cover/cover_250x350.jpg',
    slug: 'star-wars-darth-vader-2020',
  },
  {
    title: 'Miles Morales: Spider-Man (2018-)',
    link: 'https://readcomicsonline.ru/comic/miles-morales-spiderman-2018',
    img: 'https://readcomicsonline.ru/uploads/manga/miles-morales-spiderman-2018/cover/cover_250x350.jpg',
    slug: 'miles-morales-spiderman-2018',
  },
  {
    title: 'Immortal Hulk (2018-)',
    link: 'https://readcomicsonline.ru/comic/immortal-hulk-2018',
    img: 'https://readcomicsonline.ru/uploads/manga/immortal-hulk-2018/cover/cover_250x350.jpg',
    slug: 'immortal-hulk-2018',
  },
  {
    title: 'Avengers (2018-)',
    link: 'https://readcomicsonline.ru/comic/avengers-2018',
    img: 'https://readcomicsonline.ru/uploads/manga/avengers-2018/cover/cover_250x350.jpg',
    slug: 'avengers-2018',
  },
];

export function EpisodeList() {
  return <EpisodeListComp comics={comics} error={undefined} isLoading={false} />;
}
