import React from 'react';
import { render } from '@testing-library/react';
import EpisodeList from './EpisodeList';
import { comics } from './EpisodeList.stories';

describe('EpisodeList component', () => {
  it('renders the EpisodeList component', () => {
    const { getByTestId } = render(
      <EpisodeList comics={comics} error={undefined} isLoading={false} />,
    );

    expect(getByTestId('episodeList')).toBeInTheDocument();
  });
});
