import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { ComicType } from '@/pages/api/comics';
import Loader from '../Loader/Loader';
import styles from './EpisodeList.module.css';

type EpisodeListType = {
  comics: Array<ComicType> | undefined;
  error: Error | undefined;
  isLoading: boolean;
};

export default function EpisodeList({
  comics,
  error,
  isLoading,
}: EpisodeListType) {
  if (error) return <div>Failed to load comics.</div>;

  if (isLoading) return <Loader />;

  return (
    <div data-testid="episodeList">
      <h1>Latest issues</h1>

      <div>
        <Row>
          {comics && comics?.length > 0
            && comics.map(({ img, slug, title }: ComicType) => (
              <Col xs={6} md={2} key={slug}>
                <Link href="/" className={styles.episodeLink}>
                  <Image alt={title} width={250} height={350} src={img} />

                  <span className={styles.title}>{title}</span>
                </Link>
              </Col>
            ))}
        </Row>
      </div>
    </div>
  );
}
