import LoaderComp from './Loader';

const config = {
  title: 'Components/Loader',
  component: LoaderComp,
};

export default config;

export function Loader() {
  return <LoaderComp />;
}
