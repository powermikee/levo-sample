import React from 'react';
import Image from 'next/image';
import styles from './Header.module.css';

export default function Header() {
  return (
    <header className={styles.header} data-testid="header">
      <div className={styles.logo}>
        <Image
          src="/images/logo.png"
          width={132}
          height={52}
          alt="Marvel logo"
          priority
        />
      </div>
    </header>
  );
}
