import React from 'react';
import { render } from '@testing-library/react';
import Header from './Header';

describe('Header component', () => {
  it('renders the Header component', () => {
    const { getByTestId } = render(<Header />);

    expect(getByTestId('header')).toBeInTheDocument();
  });
});
