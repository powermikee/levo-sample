import HeaderComp from './Header';

const config = {
  title: 'Components/Header',
  component: HeaderComp,
};

export default config;

export function Header() {
  return <HeaderComp />;
}
