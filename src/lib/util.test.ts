import add from './util';

describe('Utils', () => {
  it('adds correctly', () => {
    expect(add(1)).toBe(2);
  });
});
