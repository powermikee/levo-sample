This is a sample project. It's a simple app that scrapes comic book data and displays it in a style similar to the Marvel comics website.

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```
